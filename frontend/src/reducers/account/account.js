import {
    REQUEST_ACCOUNT,
    RESPONSE_ACCOUNT,
    FAIL_RESPONSE_ACCOUNT,
  } from '../../actions/account/account';
  
  const initialState = {
    isLoading: false,
    isLoaded: false,
    isRequested: false,
    error: null,
    serviceData: null,
  };
  
  export default function reducer(state = initialState, action) {
    switch (action.type) {
      case REQUEST_ACCOUNT: return Object.assign({}, state, {
        isLoading: true,
        isLoaded: false,
        isRequested: true,
        error: null,
      });
  
      case RESPONSE_ACCOUNT: return Object.assign({}, state, {
        isLoading: false,
        isLoaded: true,
        isRequested: true,
        error: null,
        serviceData: (action.data || null),
      });
  
      case FAIL_RESPONSE_ACCOUNT: return Object.assign({}, state, {
        isLoading: false,
        isLoaded: false,
        isRequested: true,
        error: (action.error || new Error('Unexpected error')),
        serviceData: null,
      });
  
      default: return state;
    }
  }
  