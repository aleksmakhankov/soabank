import {
    REQUEST_VACATIONS_LIST,
    RESPONSE_VACATIONS_LIST,
    FAIL_RESPONSE_VACATIONS_LIST,
  } from '../../actions/vacations/vacations';
  
  const initialState = {
    isLoading: false,
    isLoaded: false,
    isRequested: false,
    error: null,
    serviceData: [],
  };
  
  export default function reducer(state = initialState, action) {
    switch (action.type) {
      case REQUEST_VACATIONS_LIST: return Object.assign({}, state, {
        isLoading: true,
        isLoaded: false,
        isRequested: true,
        error: null,
        serviceData: [],
      });
  
      case RESPONSE_VACATIONS_LIST: return Object.assign({}, state, {
        isLoading: false,
        isLoaded: true,
        isRequested: true,
        error: null,
        serviceData: (action.data || []),
      });
  
      case FAIL_RESPONSE_VACATIONS_LIST: return Object.assign({}, state, {
        isLoading: false,
        isLoaded: false,
        isRequested: true,
        error: (action.error || new Error('Unexpected error')),
        serviceData: [],
      });
  
      default: return state;
    }
  }
  