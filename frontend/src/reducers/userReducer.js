import {
  REQUEST_LOGIN,
  RESPONSE_LOGIN,
  FAIL_RESPONSE_LOGIN,
} from '../actions/user/login';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isRequested: false,
  error: null,
  serviceData: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOGIN: return Object.assign({}, state, {
      isLoading: true,
      isLoaded: false,
      isRequested: true,
      error: null,
      serviceData: null,
    });

    case RESPONSE_LOGIN: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: true,
      isRequested: true,
      error: null,
      serviceData: (action.data || null),
    });

    case FAIL_RESPONSE_LOGIN: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: false,
      isRequested: true,
      error: (action.error || new Error('Unexpected error')),
      serviceData: null,
    });

    default: return state;
  }
}
