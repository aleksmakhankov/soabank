import {
  REQUEST_PAYMENT_HISTORY,
  RESPONSE_PAYMENT_HISTORY,
  FAIL_RESPONSE_PAYMENT_HISTORY,
} from '../../actions/transactions/paymentHistory';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isRequested: false,
  error: null,
  serviceData: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_PAYMENT_HISTORY: return Object.assign({}, state, {
      isLoading: true,
      isLoaded: false,
      isRequested: true,
      error: null,
      serviceData: [],
    });

    case RESPONSE_PAYMENT_HISTORY: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: true,
      isRequested: true,
      error: null,
      serviceData: (action.data || []),
    });

    case FAIL_RESPONSE_PAYMENT_HISTORY: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: false,
      isRequested: true,
      error: (action.error || new Error('Unexpected error')),
      serviceData: [],
    });

    default: return state;
  }
}
