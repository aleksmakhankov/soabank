import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import userReducer from './userReducer';
import paymentHistoryReducer from './transactions/paymentHistory';
import vacationsReducer from './vacations/vacations';
import usersListReducer from './users/usersList';
import accountReducer from './account/account';

const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  user: userReducer,
  paymentHistory: paymentHistoryReducer,
  vacations: vacationsReducer,
  usersList: usersListReducer,
  account: accountReducer,
});

export default rootReducer;