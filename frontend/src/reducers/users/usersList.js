import {
  REQUEST_USERS_LIST,
  RESPONSE_USERS_LIST,
  FAIL_RESPONSE_USERS_LIST,
} from '../../actions/users/usersList';

const initialState = {
  isLoading: false,
  isLoaded: false,
  isRequested: false,
  error: null,
  serviceData: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_USERS_LIST: return Object.assign({}, state, {
      isLoading: true,
      isLoaded: false,
      isRequested: true,
      error: null,
      serviceData: [],
    });

    case RESPONSE_USERS_LIST: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: true,
      isRequested: true,
      error: null,
      serviceData: (action.data || []),
    });

    case FAIL_RESPONSE_USERS_LIST: return Object.assign({}, state, {
      isLoading: false,
      isLoaded: false,
      isRequested: true,
      error: (action.error || new Error('Unexpected error')),
      serviceData: [],
    });

    default: return state;
  }
}
