import cookie from 'react-cookies';

import { COOKIES_AUTHORIZATION_TOKEN } from '../constants';

const hasAuthorization = (NotAuthorizedComponent, AuthorizedComponent) => {
  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);

  if (!!token) {
    return AuthorizedComponent;
  }

  return NotAuthorizedComponent;
}

export default hasAuthorization;