import axios from 'axios';
import cookie from 'react-cookies';

import { clearEmptyValues, encodeValues } from '../helpers/normalizers';

import { COOKIES_AUTHORIZATION_TOKEN } from '../constants';

export const RequestType = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
};

export const objectToQueryString = queryObject =>
  Object.keys(queryObject)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(queryObject[key])}`)
    .join('&');

export const generateBasicAuthHeaderFromPassword = (username, password) => {
  if (username && username.length > 0 && password && password.length > 0) {
    const hash = btoa(`${username}:${password}`);

    return {
      Authorization: `Basic ${hash}`,
    };
  }

  return {};
};

export const generateBearerAuthHeaderFromCookie = () => {
  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);

  if (token) {
    return {
      Authorization: `Bearer ${token}`,
    };
  }

  return {};
};

export const generateCorsHeader = () => ({
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
});

export const sendRequest = (params) => {
  const {
    method, url, body, query, headers,
  } = params;

  const queryString = query && Object.keys(query).length > 0
    ? objectToQueryString(encodeValues(clearEmptyValues(query)))
    : null;

  if (!method) {
    throw new Error('Method could not be empty, please check arguments of sendRequest function');
  }

  if (!url) {
    throw new Error('Url could not be empty, please check arguments of sendRequest function');
  }

  const requestArguments = {
    method,
    // withCredentials: true,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...generateCorsHeader(),
      ...headers,
    },
  };

  if (queryString && queryString.length > 0) {
    requestArguments.url = `${url}?${decodeURI(queryString)}`;
  } else {
    requestArguments.url = url;
  }

  if (body) {
    requestArguments.data = body;
  }

  return axios(requestArguments);
};
