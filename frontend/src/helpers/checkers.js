import isEqual from 'lodash/isEqual';

export const isEmpty = value => value === '' || isEqual(value, []) || isEqual(value, {}) || typeof value === 'undefined';
