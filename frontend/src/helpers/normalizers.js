import { isEmpty } from './checkers';

export const clearEmptyValues = (data = {}) => {
  const result = {};
  for (const key of Object.keys(data)) {
    if (!isEmpty(data[key]) && !Number.isNaN(data[key])) {
      result[key] = data[key];
    }
  }
  return result;
};

export const encodeValues = (data = {}) => {
  const result = {};
  for (const key of Object.keys(data)) {
    result[key] = encodeURIComponent(data[key]);
  }
  return result;
};

