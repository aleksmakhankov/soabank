import { formValueSelector } from 'redux-form';
import cookie from 'react-cookies';
import * as crypto from 'crypto-js';

import { RequestType, generateCorsHeader, sendRequest } from '../../helpers/request';
import {
  COOKIES_AUTHORIZATION_TOKEN,
  SIGN_IN_ENDPOINT,
  USERNAME,
  PASSWORD,
  SIGN_IN_FORM,
} from '../../constants';

import createActionObject from '../';

export const REQUEST_LOGIN = '@/REQUEST_LOGIN';
export const RESPONSE_LOGIN = '@/RESPONSE_LOGIN';
export const FAIL_RESPONSE_LOGIN = '@/FAIL_RESPONSE_LOGIN';

export const requestLogin = () => createActionObject(REQUEST_LOGIN);
export const responseLogin = data => createActionObject(RESPONSE_LOGIN, { data });
export const failResponseLogin = (error) => createActionObject(FAIL_RESPONSE_LOGIN, { error });

window.fails = 0;

export const userLogin = () => async (dispatch, getState) => {
  const state = getState();
  dispatch(requestLogin());

  const selector = formValueSelector(SIGN_IN_FORM);

  const login = selector(state, USERNAME);
  const password = crypto.AES.encrypt(selector(state, PASSWORD), 'secret').toString();
  console.log('password: ', password);

  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: SIGN_IN_ENDPOINT,
      headers: { ...generateCorsHeader() },
      body: {
        login,
        password,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;
    const { token, user, role } = data;

    cookie.save(COOKIES_AUTHORIZATION_TOKEN, token, { path: '/' });

    return dispatch(responseLogin({ ...user, fullRole: role }));
  } catch (e) {
    window.fails += 1;
    window.forceUpdate();
    dispatch(failResponseLogin({ error: e }));
  }
};