import cookie from 'react-cookies';

import { COOKIES_AUTHORIZATION_TOKEN } from '../../constants';

import createActionObject from '../';

export const REQUEST_LOGIN = '@/REQUEST_LOGIN';
export const RESPONSE_LOGIN = '@/RESPONSE_LOGIN';
export const FAIL_RESPONSE_LOGIN = '@/FAIL_RESPONSE_LOGIN';

export const requestLogin = () => createActionObject(REQUEST_LOGIN);
export const responseLogin = data => createActionObject(RESPONSE_LOGIN, { data });
export const failResponseLogin = (error) => createActionObject(FAIL_RESPONSE_LOGIN, { error });

export const userLogout = () => async (dispatch, getState) => {
  try {

    localStorage.clear();
    cookie.remove(COOKIES_AUTHORIZATION_TOKEN, { path: '/' });

    return dispatch(responseLogin({}));
  } catch (e) {
    dispatch(failResponseLogin({ error: e }));
  }
};