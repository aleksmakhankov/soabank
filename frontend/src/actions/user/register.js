import { formValueSelector } from 'redux-form';
import * as crypto from 'crypto-js';

import { RequestType, generateCorsHeader, sendRequest } from '../../helpers/request';
import {
  SIGN_UP_ENDPOINT,
  USERNAME,
  PASSWORD,
  F_NAME,
  L_NAME,
  SUMM,
  COMPANY_NAME,
  SIGN_UP_FORM,
} from '../../constants';

import createActionObject from '../';

export const REQUEST_REGISTER = '@/REQUEST_REGISTER';
export const RESPONSE_REGISTER = '@/RESPONSE_REGISTER';
export const FAIL_RESPONSE_REGISTER = '@/FAIL_RESPONSE_REGISTER';

export const requestRegister = () => createActionObject(REQUEST_REGISTER);
export const responseRegister = data => createActionObject(RESPONSE_REGISTER, { data });
export const failResponseRegister = (error) => createActionObject(FAIL_RESPONSE_REGISTER, { error });

export const userRegister = () => async (dispatch, getState) => {
  const state = getState();
  dispatch(requestRegister());

  const selector = formValueSelector(SIGN_UP_FORM);

  const login = selector(state, USERNAME);
  const password = crypto.AES.encrypt(selector(state, PASSWORD), 'secret').toString();
  console.log('password: ', password);

  const first_name = selector(state, F_NAME);
  const last_name = selector(state, L_NAME);
  const rate = selector(state, SUMM);
  const company_name = selector(state, COMPANY_NAME);

  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: SIGN_UP_ENDPOINT,
      headers: { ...generateCorsHeader() },
      body: {
        login,
        password,
        first_name,
        last_name,
        rate,
        company_name,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { success } = response.data;

    return dispatch(responseRegister(success));
  } catch (e) {
    dispatch(failResponseRegister({ error: e }));
  }
};