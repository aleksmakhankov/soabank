import cookie from 'react-cookies';

import { RequestType, generateCorsHeader, generateBearerAuthHeaderFromCookie, sendRequest } from '../../helpers/request';
import {
  CREATE_TRANSACTION_BY_COMPANY_ENDPOINT,
  VACATION_ENDPOINT,
  COOKIES_AUTHORIZATION_TOKEN,
} from '../../constants';

import { getVacationList } from './vacations';

import createActionObject from '../';

export const REQUEST_PAIMENT_VACATIONS = '@/REQUEST_PAIMENT_VACATIONS';
export const RESPONSE_PAIMENT_VACATIONS = '@/RESPONSE_PAIMENT_VACATIONS';
export const FAIL_RESPONSE_PAIMENT_VACATIONS = '@/FAIL_RESPONSE_PAIMENT_VACATIONS';

export const requestPaimentVacation = () => createActionObject(REQUEST_PAIMENT_VACATIONS);
export const responsePaimentVacation = data => createActionObject(RESPONSE_PAIMENT_VACATIONS, { data });
export const failResponsePaimentVacation = (error) => createActionObject(FAIL_RESPONSE_PAIMENT_VACATIONS, { error });

export const createPaimentVacation = (vacation) => async (dispatch, getState) => {
  dispatch(requestPaimentVacation());

  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);
  
  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: CREATE_TRANSACTION_BY_COMPANY_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
      body: {
        amount: Number.parseInt(vacation.amount, 10),
        destination: vacation.destination,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data, success } = response.data;
    
    if (success) {
      await sendRequest({
        method: RequestType.POST,
        url: VACATION_ENDPOINT,
        headers: {
          ...generateCorsHeader(),
        },
        body: {
          token,
          paid: true,
	        id: vacation.id,
        },
      });
    }

    dispatch(getVacationList());
    return dispatch(responsePaimentVacation(data));
  } catch (e) {
    dispatch(failResponsePaimentVacation({ error: e }));
  }
};