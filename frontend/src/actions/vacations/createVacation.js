import cookie from 'react-cookies';
import { formValueSelector } from 'redux-form';

import { RequestType, generateCorsHeader, sendRequest } from '../../helpers/request';
import {
  VACATION_ENDPOINT,
  COOKIES_AUTHORIZATION_TOKEN,
  VACATION_FORM,
  VACATION_FROM,
  VACATION_TO,
  TO_USER,
} from '../../constants';

import { getVacationList } from './vacations';

import createActionObject from '../';

export const REQUEST_VACATIONS = '@/REQUEST_VACATIONS';
export const RESPONSE_VACATIONS = '@/RESPONSE_VACATIONS';
export const FAIL_RESPONSE_VACATIONS = '@/FAIL_RESPONSE_VACATIONS';

export const requestVacation = () => createActionObject(REQUEST_VACATIONS);
export const responseVacation = data => createActionObject(RESPONSE_VACATIONS, { data });
export const failResponseVacation = (error) => createActionObject(FAIL_RESPONSE_VACATIONS, { error });

export const createVacation = (id) => async (dispatch, getState) => {
  const state = getState();
  dispatch(requestVacation());
  
  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);

  const selector = formValueSelector(VACATION_FORM);

  const from = selector(state, VACATION_FROM);
  const to = selector(state, VACATION_TO);
  const user_id = selector(state, TO_USER);
  
  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: VACATION_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
      },
      body: {
        token,
        from,
        to,
        paid: false,
        user_id,
        id,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    dispatch(getVacationList());
    return dispatch(responseVacation(data));
  } catch (e) {
    dispatch(failResponseVacation({ error: e }));
  }
};