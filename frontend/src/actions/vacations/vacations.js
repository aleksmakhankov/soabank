import cookie from 'react-cookies';

import { RequestType, generateCorsHeader, sendRequest } from '../../helpers/request';
import { VACATIONS_LIST_ENDPOINT, COOKIES_AUTHORIZATION_TOKEN } from '../../constants';

import createActionObject from '../';

export const REQUEST_VACATIONS_LIST = '@/REQUEST_VACATIONS_LIST';
export const RESPONSE_VACATIONS_LIST = '@/RESPONSE_VACATIONS_LIST';
export const FAIL_RESPONSE_VACATIONS_LIST = '@/FAIL_RESPONSE_VACATIONS_LIST';

export const requestVacationList = () => createActionObject(REQUEST_VACATIONS_LIST);
export const responseVacationList = data => createActionObject(RESPONSE_VACATIONS_LIST, { data });
export const failResponseVacationList = (error) => createActionObject(FAIL_RESPONSE_VACATIONS_LIST, { error });

export const getVacationList = () => async (dispatch, getState) => {
  dispatch(requestVacationList());
  
  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);
  
  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: VACATIONS_LIST_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
      },
      body: {
        token,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    return dispatch(responseVacationList(data));
  } catch (e) {
    dispatch(failResponseVacationList({ error: e }));
  }
};