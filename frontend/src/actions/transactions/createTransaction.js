import { formValueSelector } from 'redux-form';

import {
  RequestType,
  generateCorsHeader,
  generateBearerAuthHeaderFromCookie,
  sendRequest,
} from '../../helpers/request';
import {
  CREATE_TRANSACTION_ENDPOINT,
  TO_USER,
  SUMM,
  TRANSACTIONS_FORM,
} from '../../constants';

import { paymentHistoryByUserRole } from './paymentHistoryByUser';

import createActionObject from '../';

export const REQUEST_CREATE_TRANSACTION = '@/REQUEST_CREATE_TRANSACTION';
export const RESPONSE_CREATE_TRANSACTION = '@/RESPONSE_CREATE_TRANSACTION';
export const FAIL_RESPONSE_CREATE_TRANSACTION = '@/FAIL_RESPONSE_CREATE_TRANSACTION';

export const requestCreateTransaction = () => createActionObject(REQUEST_CREATE_TRANSACTION);
export const responseCreateTransaction = data => createActionObject(RESPONSE_CREATE_TRANSACTION, { data });
export const failResponseCreateTransaction = (error) => createActionObject(FAIL_RESPONSE_CREATE_TRANSACTION, { error });

export const createTransaction = () => async (dispatch, getState) => {
  const state = getState();
  dispatch(requestCreateTransaction());

  const selector = formValueSelector(TRANSACTIONS_FORM);

  const amount = +selector(state, SUMM);
  const destination = +selector(state, TO_USER);

  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: CREATE_TRANSACTION_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
      body: {
        amount,
        destination,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    dispatch(paymentHistoryByUserRole());
    return dispatch(responseCreateTransaction(data));
  } catch (e) {
    dispatch(failResponseCreateTransaction({ error: e }));
  }
};