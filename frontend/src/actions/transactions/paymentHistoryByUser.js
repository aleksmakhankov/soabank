import { getPaymentHistory } from './paymentHistory';
import { getPaymentHistoryCompany } from './paymentHistoryCompany';

export const paymentHistoryByUserRole = () => async (dispatch, getState) => {
  const state = getState();
  const user = state.user.serviceData;

  switch (user.role) {
    case 2: {
      dispatch(getPaymentHistoryCompany());
      break;
    }
    default: {
      dispatch(getPaymentHistory());
      break;
    }
  }
};