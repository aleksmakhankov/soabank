import {
  RequestType,
  generateCorsHeader,
  generateBearerAuthHeaderFromCookie,
  sendRequest,
} from '../../helpers/request';
import { PAYMENT_HISTORY_ENDPOINT } from '../../constants';

import createActionObject from '../';

export const REQUEST_PAYMENT_HISTORY = '@/REQUEST_PAYMENT_HISTORY';
export const RESPONSE_PAYMENT_HISTORY = '@/RESPONSE_PAYMENT_HISTORY';
export const FAIL_RESPONSE_PAYMENT_HISTORY = '@/FAIL_RESPONSE_PAYMENT_HISTORY';

export const requestPaymentHistory = () => createActionObject(REQUEST_PAYMENT_HISTORY);
export const responsePaymentHistory = data => createActionObject(RESPONSE_PAYMENT_HISTORY, { data });
export const failResponsePaymentHistory = (error) => createActionObject(FAIL_RESPONSE_PAYMENT_HISTORY, { error });

export const getPaymentHistory = () => async (dispatch, getState) => {
  dispatch(requestPaymentHistory());

  try {
    const response = await sendRequest({
      method: RequestType.GET,
      url: PAYMENT_HISTORY_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    return dispatch(responsePaymentHistory(data));
  } catch (e) {
    dispatch(failResponsePaymentHistory({ error: e }));
  }
};