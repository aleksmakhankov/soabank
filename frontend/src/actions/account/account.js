import { RequestType, generateCorsHeader, sendRequest, generateBearerAuthHeaderFromCookie } from '../../helpers/request';
import { ACCOUNT_ENDPOINT } from '../../constants';

import createActionObject from '../';

export const REQUEST_ACCOUNT = '@/REQUEST_ACCOUNT';
export const RESPONSE_ACCOUNT = '@/RESPONSE_ACCOUNT';
export const FAIL_RESPONSE_ACCOUNT = '@/FAIL_RESPONSE_ACCOUNT';

export const requestAccount = () => createActionObject(REQUEST_ACCOUNT);
export const responseAccount = data => createActionObject(RESPONSE_ACCOUNT, { data });
export const failResponseAccount = (error) => createActionObject(FAIL_RESPONSE_ACCOUNT, { error });

export const getAccount = () => async (dispatch, getState) => {
  dispatch(requestAccount());
  
  try {
    const response = await sendRequest({
      method: RequestType.GET,
      url: ACCOUNT_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    return dispatch(responseAccount(data));
  } catch (e) {
    dispatch(failResponseAccount({ error: e }));
  }
};