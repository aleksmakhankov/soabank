const createActionObject = (type, props = {}) => ({
  type,
  ...props,
});

export default createActionObject;