import {
  RequestType,
  generateCorsHeader,
  generateBearerAuthHeaderFromCookie,
  sendRequest,
} from '../../helpers/request';
import { USERS_LIST_ENDPOINT } from '../../constants';

import { getUsersEmployees } from './usersEmployees';

import createActionObject from '../';

export const REQUEST_USERS_LIST = '@/REQUEST_USERS_LIST';
export const RESPONSE_USERS_LIST = '@/RESPONSE_USERS_LIST';
export const FAIL_RESPONSE_USERS_LIST = '@/FAIL_RESPONSE_USERS_LIST';

export const requestUsersList = () => createActionObject(REQUEST_USERS_LIST);
export const responseUsersList = data => createActionObject(RESPONSE_USERS_LIST, { data });
export const failResponseUsersList = (error) => createActionObject(FAIL_RESPONSE_USERS_LIST, { error });

export const getUsersList = () => async (dispatch, getState) => {
  dispatch(requestUsersList());

  try {
    const response = await sendRequest({
      method: RequestType.GET,
      url: USERS_LIST_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { data } = response.data;

    return dispatch(getUsersEmployees(data));
  } catch (e) {
    dispatch(failResponseUsersList({ error: e }));
  }
};