import { formValueSelector } from 'redux-form';

import {
  RequestType,
  generateCorsHeader,
  generateBearerAuthHeaderFromCookie,
  sendRequest,
} from '../../helpers/request';
import {
  INVITE_ENDPOINT,
  USERNAME,
  PASSWORD,
  F_NAME,
  L_NAME,
  ROLE,
  COMPANY_NAME,
  SIGN_UP_FORM,
} from '../../constants';

import createActionObject from '../';

export const REQUEST_INVITE = '@/REQUEST_INVITE';
export const RESPONSE_INVITE = '@/RESPONSE_INVITE';
export const FAIL_RESPONSE_INVITE = '@/FAIL_RESPONSE_INVITE';

export const requestInvite = () => createActionObject(REQUEST_INVITE);
export const responseInvite = data => createActionObject(RESPONSE_INVITE, { data });
export const failResponseInvite = (error) => createActionObject(FAIL_RESPONSE_INVITE, { error });

export const userInvite = () => async (dispatch, getState) => {
  const state = getState();
  dispatch(requestInvite());

  const selector = formValueSelector(SIGN_UP_FORM);

  const login = selector(state, USERNAME);
  const password = selector(state, PASSWORD);
  const first_name = selector(state, F_NAME);
  const last_name = selector(state, L_NAME);
  const company_name = selector(state, COMPANY_NAME);
  const role = selector(state, ROLE);

  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: INVITE_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
      body: {
        login,
        password,
        name: `${first_name} ${last_name}`,
        company_name,
        role,
      },
    });

    if (!response) {
      throw new Error('Could not send request');
    }

    const { success } = response.data;

    return dispatch(responseInvite(success));
  } catch (e) {
    dispatch(failResponseInvite({ error: e }));
  }
};