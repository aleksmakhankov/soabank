import cookie from 'react-cookies';

import {
  RequestType,
  generateCorsHeader,
  generateBearerAuthHeaderFromCookie,
  sendRequest,
} from '../../helpers/request';
import { USERS_EMPLOYEES_ENDPOINT, COOKIES_AUTHORIZATION_TOKEN } from '../../constants';

import {
  responseUsersList,
  failResponseUsersList,
} from './usersList';

export const getUsersEmployees = (users) => async (dispatch, getState) => {
  const token = cookie.load(COOKIES_AUTHORIZATION_TOKEN);
  
  try {
    const response = await sendRequest({
      method: RequestType.POST,
      url: USERS_EMPLOYEES_ENDPOINT,
      headers: {
        ...generateCorsHeader(),
        ...generateBearerAuthHeaderFromCookie(),
      },
      body: { token },
    });
    
    if (!response) {
      throw new Error('Could not send request');
    }
    
    const { data } = response.data;
    const usersWithRates = users.map(user => {
      const userWithRates = data.find(item => item.user_id === user.id) || {};
      
      return {
        ...user,
        rate: userWithRates.rate ,
      };
    });

    return dispatch(responseUsersList(usersWithRates));
  } catch (e) {
    dispatch(failResponseUsersList({ error: e }));
  }
};