export const required = value => (value || typeof value === 'number' ? undefined : 'Обязательное поле')

export const minLength = min => value =>
  value && value.length < min ? `Минимум ${min} символом или больше` : undefined

export const minLengthPassword = minLength(6)

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Неверный email адрес'
    : undefined

export const alphaNumeric = value =>
  value && hasChars(value) && hasNumber
    ? 'Пароль должен состоять из чисел и букв'
    : undefined

function hasNumber(myString) {
    return /\d/.test(myString);
}

function hasChars(myString) {
    return /^[a-zA-Z]+$/.test(myString);
}