export const API_PROTOCOL = 'http';
export const API_URL = 'localhost';

export const TRANSACTIONS_API_PORT = '5000';
export const VACATIONS_API_PORT = '5001';

export const TRANSACTIONS_HOST = `${API_PROTOCOL}://${API_URL}:${TRANSACTIONS_API_PORT}`;
export const VACATIONS_HOST = `${API_PROTOCOL}://${API_URL}:${VACATIONS_API_PORT}`;

export const TRANSACTIONS_API_HOST = `${TRANSACTIONS_HOST}/api`;
export const VACATIONS_API_HOST = `${VACATIONS_HOST}/api`;

// Authorization flow
export const SIGN_IN_ENDPOINT = `${VACATIONS_API_HOST}/authorization`;
export const SIGN_UP_ENDPOINT = `${VACATIONS_API_HOST}/registration`;

// Transactions
export const PAYMENT_HISTORY_ENDPOINT = `${TRANSACTIONS_API_HOST}/payment-history`;
export const PAYMENT_HISTORY_COMPANY_ENDPOINT = `${TRANSACTIONS_API_HOST}/payment-history-company`;
export const CREATE_TRANSACTION_ENDPOINT = `${TRANSACTIONS_API_HOST}/charge`;
export const CREATE_TRANSACTION_BY_COMPANY_ENDPOINT = `${TRANSACTIONS_API_HOST}/charge-by-company`;

// Vacations
export const VACATIONS_LIST_ENDPOINT = `${VACATIONS_API_HOST}/vacations`;
export const VACATION_ENDPOINT = `${VACATIONS_API_HOST}/vacation`;

// Users
export const USERS_LIST_ENDPOINT = `${TRANSACTIONS_API_HOST}/list`;
export const USERS_EMPLOYEES_ENDPOINT = `${VACATIONS_API_HOST}/employees`;

// Account
export const ACCOUNT_ENDPOINT = `${TRANSACTIONS_API_HOST}/account`;

// Invite
export const INVITE_ENDPOINT = `${TRANSACTIONS_API_HOST}/invite`;