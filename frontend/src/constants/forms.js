export const SIGN_IN_FORM = 'signInForm';
export const SIGN_UP_FORM = 'signUpForm';
export const TRANSACTIONS_FORM = 'transactionsForm';
export const INVITE_USER_FORM = 'inviteUserForm';
export const VACATION_FORM = 'vacationForm';
