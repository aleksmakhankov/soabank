export const COOKIES_AUTHORIZATION_TOKEN = 'bank-secret';
export const ROLES = [
  {
    label: 'Клиент',
    value: 1,
  },
  {
    label: 'Банкир',
    value: 2,
  },
  {
    label: 'Бухгалтер',
    value: 3,
  },
];