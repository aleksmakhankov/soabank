export const USERNAME = 'username';
export const PASSWORD = 'password';
export const NAME = 'name';
export const F_NAME = 'fName';
export const L_NAME = 'lName';
export const ROLE = 'role';
export const COMPANY_NAME = 'companyName';

export const FROM_USER = 'fromUser';
export const TO_USER = 'toUser';
export const SUMM = 'summ';

// Vacation fields
export const VACATION_FROM = 'from';
export const VACATION_TO = 'to';
