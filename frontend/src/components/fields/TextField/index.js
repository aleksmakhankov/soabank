import React from 'react';
import { TextField } from 'react-md';

class Input extends React.Component {
  render() {
    const { meta: { touched, error, warning }, input, componentProps, type } = this.props;

    return (
      <TextField
        {...componentProps}
        {...input}
        error={touched && !!(error || warning)}
        errorText={error || warning}
        type={type}
      />
    );
  }
}

Input.defaultProps = {
  type: 'text',
};

export default Input;
