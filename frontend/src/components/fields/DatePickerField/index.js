import React from 'react';
import moment from 'moment';
import { DatePicker } from 'react-md';

class Date extends React.Component {
  constructor(props) {
    super(props);

    const initialValue = props.componentProps.defaultValue;
    this.state = {
      value: initialValue ? moment(initialValue, 'MM/DD/YYYY').format('DD.MM.YYYY') : '',
    }
  }
  onChange = (dateString, dateObject, event) => {
    const { input: { onChange } } = this.props;
    const value = moment(dateObject).format('DD.MM.YYYY');
    this.setState({ value }, () => onChange(value));
  }
  render() {
    const { meta: { touched, error, warning }, input, componentProps } = this.props;

    return (
      <DatePicker
        {...componentProps}
        {...input}
        value={this.state.value ? moment(this.state.value, 'DD.MM.YYYY').format("MM/DD/YYYY") : ''}
        onChange={this.onChange}
        displayMode="portrait"
        error={touched && !!(error || warning)}
        errorText={error || warning}
      />
    );
  }
}

export default Date;
