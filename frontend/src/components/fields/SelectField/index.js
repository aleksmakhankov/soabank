import React from 'react';
import { SelectField } from 'react-md';

class Select extends React.Component {
  render() {
    const { meta: { touched, error, warning }, input, componentProps } = this.props;

    return (
      <SelectField
        simplifiedMenu
        error={touched && !!(error || warning)}
        errorText={error || warning}
        {...componentProps}
        {...input}
      />
    );
  }
}

Select.defaultProps = {
  componentProps: {
    position: SelectField.Positions.BELOW,
  }
};

export default Select;
