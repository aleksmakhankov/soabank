import React from 'react';
import { NavLink } from "react-router-dom";
import { FontIcon, ListItem } from "react-md";
import { ROUTE_VACATION, ROUTE_TRANSACTIONS } from '../../../constants';

const allRoutes = [
    {
        id: 1,
        route: ROUTE_VACATION,
        label: 'График отпусков',
        icon: 'work',
    },
    {
        id: 2,
        route: ROUTE_TRANSACTIONS,
        label: 'Транзакции',
        icon: 'account_balance_wallet',
    }
]

export default role => {
  switch (role) {
    default: {
      return allRoutes.map(({ id, route, label, icon }) => (
          <NavLink className="link" key={id} to={route}>
              <ListItem
                leftIcon={<FontIcon key={icon}>{icon}</FontIcon>}
                primaryText={label}
            />
          </NavLink>
      ));
    }
  }
};
