import React from "react";
import { Toolbar, Button, Drawer, Avatar, Divider } from "react-md";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { getAccount } from "../../../actions/account/account";
import { userLogout } from "../../../actions/user/logout";
import generateItems from "./navItems";

import "./styles.scss";

class GeneralLayout extends React.Component {
  state = {
    drawerVisible: false
  };

  componentWillMount() {
    const { getAccount } = this.props;

    getAccount();
  }

  onLogout = () => {
    const {
      onLogout,
      history: { push }
    } = this.props;

    onLogout().then(() => {
      push("/");
    });
  };

  closeDrawer = () => {
    this.setState({ drawerVisible: false });
  };

  openDrawer = () => {
    this.setState({ drawerVisible: true });
  };

  handleVisibility = drawerVisible => {
    this.setState({ drawerVisible });
  };

  render() {
    const { drawerVisible } = this.state;
    const { user, account, getAccount } = this.props;

    return (
      <React.Fragment>
        <Toolbar
          colored
          nav={
            <Button icon onClick={this.openDrawer}>
              menu
            </Button>
          }
          title="Финансы V0.1"
          actions={
            <Button icon onClick={this.onLogout}>
              exit_to_app
            </Button>
          }
        />
        <Drawer
          id="app-drawer"
          type={Drawer.DrawerTypes.TEMPORARY}
          visible={drawerVisible}
          position="left"
          navItems={generateItems()}
          onVisibilityChange={this.handleVisibility}
          header={
            <div>
              <Toolbar
                title="Меню"
                actions={
                  <Button icon onClick={this.closeDrawer}>
                    arrow_back
                  </Button>
                }
                className="md-divider-border md-divider-border--bottom"
              />
              {user && user.name ? (
                <div className="user-info">
                  <Avatar>{user.name[0].toUpperCase()}</Avatar>
                  <span className="name">{user.name}</span>
                  <span className="company">{user.company_name}</span>
                  <span className="role">{user.fullRole.label}</span>
                  {account ? (
                    <React.Fragment>
                      <span className="account">
                        Текущий счёт: <span>{account.amount}</span> BYN.
                      </span>
                      {account && account.company.amount ? (
                        <span className="account-company">
                          Счёт компании: <span>{account.company.amount}</span>{" "}
                          BYN.
                        </span>
                      ) : null}
                      <Button
                        primary
                        flat
                        onClick={getAccount}
                        label="Обновить информацию"
                      />
                    </React.Fragment>
                  ) : null}
                </div>
              ) : null}
              <Divider />
            </div>
          }
        />
        {this.props.children}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.serviceData,
  account: state.account.serviceData
});

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(userLogout()),
  getAccount: () => dispatch(getAccount())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(GeneralLayout));
