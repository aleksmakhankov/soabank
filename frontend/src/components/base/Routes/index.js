import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import SignInPage from '../../pages/SignInPage';
import SignUpPage from '../../pages/SignUpPage';
import TransactionsPage from '../../pages/TransactionsPage';
import VacationPage from '../../pages/VacationPage';

import hasAuthorization from '../../../helpers/hasAuthorization';

import {
  ROUTE_SIGN_IN,
  ROUTE_SIGN_UP,
  ROUTE_TRANSACTIONS,
  ROUTE_VACATION,
} from '../../../constants';

const redirectToLogin = () => <Redirect to={ROUTE_SIGN_IN} />;
const redirectToTransactions = () => <Redirect to={ROUTE_TRANSACTIONS} />;

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Route exact path="/" component={hasAuthorization(redirectToLogin, redirectToTransactions)} />
        <Route exact path={ROUTE_SIGN_IN} component={SignInPage} />
        <Route exact path={ROUTE_SIGN_UP} component={SignUpPage} />
        <Route exact path={ROUTE_TRANSACTIONS} component={hasAuthorization(redirectToLogin, TransactionsPage)} />
        <Route exact path={ROUTE_VACATION} component={hasAuthorization(redirectToLogin, VacationPage)} />
      </React.Fragment>
    );
  }
}

export default App;
