import React from 'react';

import SignInForm from '../../forms/SignInForm';

import './style.scss';

class SignInPage extends React.Component {
  render() {
    return (
      <div className="sign-in-page">
        <SignInForm />
      </div>
    );
  }
}

export default SignInPage;