import React, { Component } from "react";
import { compose } from 'redux';
import { connect } from 'react-redux';
import BigCalendar from "react-big-calendar";
import { Button } from "react-md";
import moment from "moment";

import GeneralLayout from "../../base/GeneralLayout";
import { getVacationList } from "../../../actions/vacations/vacations";
import { createPaimentVacation } from "../../../actions/vacations/paimentVacation";
import { getUsersList } from "../../../actions/users/usersList";

import VacationModal from './vacationModal';

import "react-big-calendar/lib/css/react-big-calendar.css";
import "./styles.scss";

const localizer = BigCalendar.momentLocalizer(moment);

const formatEvents = (users, vacations, loggedUser) => {
  const events = [];
  
  if (!users || !vacations || users.lenght === 0 || vacations.length === 0) {
    return events;
  }

  vacations.forEach(vacation => {
    const user = users.find(user => +user.id === +vacation.user_id) || {};

    const start = moment(vacation.from, 'DD.MM.YYYY');
    const end = moment(vacation.to, 'DD.MM.YYYY');
    const days = end.diff(start, 'days');

    const vacationSumm = (((user.rate || 0) * days) / 22).toFixed(0);

    if (loggedUser.company_name === user.company_name) {
      events.push({
        id: vacation.id,
        title: `Отпуск для ${(user || {}).name} (ко-во дней - ${days}) (отпускные - ${vacationSumm} BYN) (${vacation.paid ? 'Оплачен' : 'Не оплачен'})`,
        start,
        end,
      });
    }
  })

  return events;
}

export class VacationPage extends Component {
  state = {
    visibleModal: false,
    selectedVacation: null,
  }

  componentWillMount() {
    const {
      onGetVacationList,
      onGetUsersList,
    } = this.props;

    onGetVacationList();
    onGetUsersList();
  }

  handleCloseModal = () => {
    this.setState({
      visibleModal: false,
      selectedVacation: null,
    });
  }

  handleOpenModal = (vacation) => {
    const { user } = this.props;
    const selectedVacation = vacation && vacation.id ? vacation : {};
    const vacationFormValues = selectedVacation && selectedVacation.from
      ? {
        from: moment(selectedVacation.from, 'DD.MM.YYYY').format('MM/DD/YYYY'),
        to: moment(selectedVacation.to, 'DD.MM.YYYY').format('MM/DD/YYYY'),
        toUser: Number.parseInt(selectedVacation.user_id),
        id: selectedVacation.id,
      }
      : {
        from: '',
        to: '',
        toUser: user.id,
      };
    this.setState({ visibleModal: true, selectedVacation: vacationFormValues });
  }

  handleSelectEvent = (event) => {
    const { user, users, vacations, onPaidVacation } = this.props;
    const { id } = event;
    const vacation = vacations.find(vacation => vacation.id === id);
    const userByVacation = users.find(user => +user.id === +vacation.user_id) || {};

    const start = moment(vacation.from, 'DD.MM.YYYY');
    const end = moment(vacation.to, 'DD.MM.YYYY');
    const days = end.diff(start, 'days');

    const vacationSumm = (((userByVacation.rate || 0) * days) / 22).toFixed(0);

    if (!vacation.paid) {
      if (user.role === 3) {
        const userAnswer = window.confirm(`Оплатить отпуск ${userByVacation.name}, в размере ${vacationSumm} BYN?`);
        if (userAnswer) {
          onPaidVacation({
            amount: vacationSumm,
            destination: userByVacation.id,
            id: vacation.id,
          });
        }
      } else {
        if (vacation) {
          this.handleOpenModal(vacation);
        }
      }
    }
  }

  render() {
    const { vacations, users, user } = this.props;
    const { visibleModal, selectedVacation } = this.state;

    return (
      <GeneralLayout>
        <div className="container">
          <h3 className="md-title title">График отпусков:</h3>
          <div className="toolbar">
            <Button raised primary onClick={this.handleOpenModal}>Добавить отпуск</Button>
          </div>
          <BigCalendar
            views={{ month: true }}
            localizer={localizer}
            events={formatEvents(users, vacations, user)}
            onSelectEvent={this.handleSelectEvent}
            startAccessor="start"
            endAccessor="end"
          />
        </div>
        <VacationModal
          visible={visibleModal}
          onClose={this.handleCloseModal}
          {...(selectedVacation ? ({
            title: 'Редактировать отпуск',
            initialValues: selectedVacation,
          }) : {})}
          />
      </GeneralLayout>
    );
  }
}

const mapStateToProps = state => ({
  vacations: state.vacations.serviceData,
  users: state.usersList.serviceData,
  user: state.user.serviceData,
});

const mapDispatchToProps = dispatch => ({
  onGetVacationList: () => dispatch(getVacationList()),
  onGetUsersList: () => dispatch(getUsersList()),
  onPaidVacation: (data) => dispatch(createPaimentVacation(data)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(VacationPage);
