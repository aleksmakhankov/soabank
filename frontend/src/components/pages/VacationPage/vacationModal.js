import React, { PureComponent } from "react";
import { DialogContainer } from "react-md";

import VacationForm from "../../forms/VacationForm";

export default class SimpleModal extends PureComponent {
  render() {
    const { visible, title, onClose, initialValues } = this.props;

    return (
      <DialogContainer
        id="vacation-modal"
        visible={visible}
        onHide={onClose}
        title={title || 'Добавление отпуска'}
      >
        <VacationForm onClose={onClose} isModal {...(initialValues ? { initialValues } : {})} />
      </DialogContainer>
    );
  }
}
