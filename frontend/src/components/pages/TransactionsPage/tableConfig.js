const config = [
  {
    name: 'Дата транзакции',
    field: 'history.created_at',
    type: 'date',
  },
  {
    name: 'Отправитель',
    field: 'source.name',
    subField: 'source.company_name',
  },
  {
    name: 'Получатель',
    field: 'target.name',
    subField: 'target.company_name',
  },
  {
    name: 'Сумма',
    field: 'history.amount',
    type: 'summ',
  },
];

export default config;