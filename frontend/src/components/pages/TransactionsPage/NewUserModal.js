import React, { PureComponent } from 'react';
import { Button, DialogContainer } from 'react-md';

import SignUpForm from '../../forms/SignUpForm';

export default class SimpleModal extends PureComponent {
  state = { visible: false };

  show = () => {
    this.setState({ visible: true });
  };

  hide = () => {
    this.setState({ visible: false });
  };

  render() {
    const { visible } = this.state;

    return (
      <div>
        <Button raised primary onClick={this.show}>Создать пользователя</Button>
        <DialogContainer
          id="create-user-modal"
          visible={visible}
          onHide={this.hide}
          title="Создание пользователя"
        >
          <SignUpForm
            isModal
            onClose={this.hide}
          />
        </DialogContainer>
      </div>
    );
  }
}