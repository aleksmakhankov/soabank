import React, { PureComponent } from 'react';
import { Button, DialogContainer } from 'react-md';

import TransactionForm from '../../forms/TransactionForm';

export default class SimpleModal extends PureComponent {
  state = { visible: false };

  show = () => {
    this.setState({ visible: true });
  };

  hide = () => {
    this.setState({ visible: false });
  };

  render() {
    const { visible } = this.state;

    return (
      <div>
        <Button raised primary onClick={this.show}>Совершить транзакцию</Button>
        <DialogContainer
          id="create-user-modal"
          visible={visible}
          onHide={this.hide}
          title="Совершить транзакцию"
        >
          <TransactionForm
            onClose={this.hide}
          />
        </DialogContainer>
      </div>
    );
  }
}