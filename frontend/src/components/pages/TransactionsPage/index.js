import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import get from "lodash/get";
import * as moment from "moment";
import {
  DataTable,
  TableHeader,
  TableRow,
  TableColumn,
  TableBody,
  Switch,
} from "react-md";

import GeneralLayout from "../../base/GeneralLayout";
import NewUserModal from "./NewUserModal";
import NewTransactionModal from "./NewTransactionModal";

import { getPaymentHistory } from "../../../actions/transactions/paymentHistory";
import { getPaymentHistoryCompany } from "../../../actions/transactions/paymentHistoryCompany";

import config from "./tableConfig";

import "./style.scss";

class TransactionsPage extends React.Component {
  state = {
    transactionSelf: true,
  }

  componentWillMount() {
    const { getPaymentHistory } = this.props;
    getPaymentHistory();
  }

  renderByType = (element, config) => {
    const { field, type } = config;

    const value = get(element, field, "");

    if (type === "date") {
      return moment(value).format("DD/MM/YYYY HH:mm");
    }
    if (type === 'summ') {
      return `${value} BYN`;
    }

    return value === '-' ? 'Компания' : value;
  };

  handleToggleTransactionType = () => {
    this.setState(prevState => {
      const { getPaymentHistory, getPaymentHistoryCompany } = this.props;
      const { transactionSelf } = prevState;
      
      if (!transactionSelf) {
        getPaymentHistory();
      } else {
        getPaymentHistoryCompany();
      }
  
      return { transactionSelf: !transactionSelf };
    });
  }

  render() {
    const { data, user } = this.props;
    const { transactionSelf } = this.state;

    return (
      <GeneralLayout>
        <div className="transactions-page">
          <div className="transactions-page__controls">
            <NewTransactionModal />
            {user && user.role === 2 && (
              <NewUserModal />
            )}
            <Switch
              id="switch-transaction"
              type="switch"
              label={transactionSelf ? 'Персональная история транзакций' : 'История транзакций сотрудников компаний'}
              name="transaction-type"
              onChange={this.handleToggleTransactionType}
              disabled={user && user.role !== 2}
            />
          </div>
          <DataTable plain>
            <TableHeader>
              <TableRow>
                {config.map((item, position) => (
                  <TableColumn key={`name-${item.name}`}>
                    {item.name}
                  </TableColumn>
                ))}
              </TableRow>
            </TableHeader>
            <TableBody>
              {data.map((row, i) => (
                <TableRow key={i}>
                  {config.map((item, position) => (
                    <TableColumn key={`data-${i}-${position}`}>
                      <b>{this.renderByType(row, item)}</b>
                      {item.subField
                        ? ` (${get(row, item.subField, "")})`
                        : null}
                    </TableColumn>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </DataTable>
        </div>
      </GeneralLayout>
    );
  }
}

const mapStateToProps = state => ({
  data: state.paymentHistory.serviceData || [],
  user: state.user.serviceData || []
});
const mapDispatchToProps = dispatch => ({
  getPaymentHistory: () => dispatch(getPaymentHistory()),
  getPaymentHistoryCompany: () => dispatch(getPaymentHistoryCompany())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(TransactionsPage));
