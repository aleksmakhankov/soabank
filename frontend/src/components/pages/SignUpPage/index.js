import React from 'react';

import SignUpForm from '../../forms/SignUpForm';

import './style.scss';

class SignUpPage extends React.Component {
  render() {
    return (
      <div className="sign-up-page">
        <SignUpForm />
      </div>
    );
  }
}

export default SignUpPage;