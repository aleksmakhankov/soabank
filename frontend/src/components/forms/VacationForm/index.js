import React from "react";
import { withRouter } from "react-router-dom";
import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import { Button } from "react-md";

import DatePickerField from "../../fields/DatePickerField";
import SelectField from '../../fields/SelectField';

import {
  VACATION_FORM,
  VACATION_FROM,
  VACATION_TO,
  TO_USER,
} from "../../../constants";

import { getUsersList } from '../../../actions/users/usersList';
import { createVacation } from '../../../actions/vacations/createVacation';

import "./style.scss";

class VacationForm extends React.Component {
  componentWillMount() {
    const { onGetUsers } = this.props;

    onGetUsers();
  }
  onSubmit = () => {
    const {
      onCreateVaction,
      onClose,
      initialValues,
    } = this.props;

    onCreateVaction(initialValues.id)
      .then(action => {
        if (action && action.data) {
          onClose();
        }
      });
  };

  render() {
    const {
      submitting,
      invalid,
      pristine,
      handleSubmit,
      initialValues,
      users,
      user,
    } = this.props;
    const usersOptions = users.map((user) => ({
      label: user.role === 4 ? `Компания ${user.company_name}` : `${user.name} (${user.company_name})`,
      value: user.id,
    }));
    return (
      <form className="vacation" onSubmit={handleSubmit(this.onSubmit)} initialValues={initialValues}>
        <Field
          name={VACATION_FROM}
          component={DatePickerField}
          componentProps={{
            defaultValue: initialValues.from,
            minDate: new Date(),
            label: "Начало отпуска (с)",
            lineDirection: "center"
          }}
        />
        <Field
          name={VACATION_TO}
          component={DatePickerField}
          componentProps={{
            defaultValue: initialValues.to,
            minDate: new Date(),
            label: "Конец отпуска (до)",
            lineDirection: "center"
          }}
        />
        <Field
          name={TO_USER}
          component={SelectField}
          componentProps={{
            disabled: user.role !== 3,
            menuItems: usersOptions,
            label: 'Сотрудник',
            id: 'to-user-field',
          }}
        />
        <div className="controls">
          <Button
            raised
            primary
            type="submit"
            disabled={invalid || pristine || submitting}
          >
            Добавить
          </Button>
        </div>
      </form>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values[VACATION_FROM]) {
    errors[VACATION_FROM] = "Обязательное поле";
  }
  if (!values[VACATION_TO]) {
    errors[VACATION_TO] = "Обязательное поле";
  }
  if (!values[TO_USER]) {
    errors[TO_USER] = "Обязательное поле";
  }
  return errors;
};

const form = reduxForm({
  form: VACATION_FORM,
  validate,
  enableReinitialize: true,
})(withRouter(VacationForm));

const mapStateToProps = (state) => ({
  user: state.user.serviceData,
  users: state.usersList.serviceData,
});
const mapDispatchToProps = dispatch => ({
  onGetUsers: () => dispatch(getUsersList()),
  onCreateVaction: (id) => dispatch(createVacation(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(form);
