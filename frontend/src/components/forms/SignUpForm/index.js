import React from 'react';
import { withRouter } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { Card, CardTitle, CardText, Button } from 'react-md';

import TextField from '../../fields/TextField';
import SelectField from '../../fields/SelectField';

import { required, minLengthPassword, alphaNumeric, email } from "../../../constants/validators";
import { userRegister } from '../../../actions/user/register';
import { userInvite } from '../../../actions/users/inviteUser';

import {
  SIGN_UP_FORM,
  USERNAME,
  PASSWORD,
  F_NAME,
  L_NAME,
  ROLE,
  SUMM,
  COMPANY_NAME,
  ROUTE_SIGN_IN,
  ROLES,
} from '../../../constants';

import './style.scss';

class SignInForm extends React.Component {
  componentDidMount() {
    const { change } = this.props;

    change(ROLE, ROLES[0].value);
  }
  onSubmit = () => {
    const {
      isModal,
      onInvite,
      onRegister,
      history: { push },
      reset,
      onClose,
    } = this.props;

    (isModal ? onInvite : onRegister)().then((action) => {
      if (!isModal) {
        if (action && action.data) {
          push(ROUTE_SIGN_IN);
        }
      } else {
        onClose();
        reset();
      }
    });
  }

  render() {
    const { submitting, invalid, pristine, handleSubmit, isModal } = this.props;
    return (
      <Card className="sign-up-form">
        {!isModal && (<CardTitle title="Регистрация" />)}
        <CardText>
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <Field
              name={USERNAME}
              component={TextField}
              validate={[required, email]}
              componentProps={{
                label: 'Имя пользователя',
                lineDirection: 'center',
              }}
            />
            <Field
              name={PASSWORD}
              type="password"
              component={TextField}
              validate={[required, minLengthPassword, alphaNumeric]}
              componentProps={{
                label: 'Пароль',
                lineDirection: 'center',
              }}
            />
            <Field
              name={F_NAME}
              component={TextField}
              validate={[required]}
              componentProps={{
                label: 'Имя',
                lineDirection: 'center',
              }}
            />
            <Field
              name={L_NAME}
              validate={[required]}
              component={TextField}
              componentProps={{
                label: 'Фамилия',
                lineDirection: 'center',
              }}
            />
            <Field
              name={COMPANY_NAME}
              validate={[required]}
              component={TextField}
              componentProps={{
                label: 'Название компании',
                lineDirection: 'center',
              }}
            />
            <Field
              name={SUMM}
              validate={[required]}
              component={TextField}
              componentProps={{
                label: 'Ставка',
                lineDirection: 'center',
              }}
            />
            {isModal && (
              <Field
                name={ROLE}
                component={SelectField}
                componentProps={{
                  placeholder: 'Роль пользователя',
                  menuItems: ROLES,
                  id: 'select-role-field'
                }}
              />
            )}
            <div className="controls">
              {!isModal && (
                <NavLink to={ROUTE_SIGN_IN}>
                  <Button raised secondary>
                    Уже зарегистрирован
                  </Button>
                </NavLink>
              )}
              <Button
                raised
                primary
                type="submit"
                disabled={invalid || pristine || submitting}
              >
                Регистрация
              </Button>
            </div>
          </form>
        </CardText>
      </Card>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values[USERNAME]) {
    errors[USERNAME] = 'Обязательное поле';
  }
  if (!values[PASSWORD]) {
    errors[PASSWORD] = 'Обязательное поле';
  }
  if (!values[F_NAME]) {
    errors[F_NAME] = 'Обязательное поле';
  }
  if (!values[L_NAME]) {
    errors[L_NAME] = 'Обязательное поле';
  }
  if (!values[COMPANY_NAME]) {
    errors[COMPANY_NAME] = 'Обязательное поле';
  }
  if (!values[SUMM]) {
    errors[SUMM] = 'Обязательное поле';
  }
  return errors;
}

const form = reduxForm({
  form: SIGN_UP_FORM,
  validate,
})(withRouter(SignInForm));

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
  onRegister: () => dispatch(userRegister()),
  onInvite: () => dispatch(userInvite()),
});

export default connect(mapStateToProps, mapDispatchToProps)(form)