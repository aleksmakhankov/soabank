import React from 'react';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { Card, CardText, Button } from 'react-md';

import TextField from '../../fields/TextField';
import SelectField from '../../fields/SelectField';

import { createTransaction } from '../../../actions/transactions/createTransaction';
import { getUsersList } from '../../../actions/users/usersList';

import {
  TRANSACTIONS_FORM,
  TO_USER,
  SUMM,
} from '../../../constants';

import './style.scss';

class TransactionsForm extends React.Component {
  componentWillMount() {
    const { onGetUsers } = this.props;

    onGetUsers();
  }
  onSubmit = () => {
    const {
      onCreateTransaction,
      onClose,
      reset,
    } = this.props;

    onCreateTransaction().then(() => {
      onClose();
      reset();
    });
  }

  render() {
    const { submitting, invalid, pristine, handleSubmit, users } = this.props;
    const usersOptions = users.map((user) => ({
      label: user.role === 4 ? `Компания ${user.company_name}` : `${user.name} (${user.company_name})`,
      value: user.id,
    }));
    return (
      <Card className="transactions-form">
        <CardText>
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <Field
              name={TO_USER}
              component={SelectField}
              componentProps={{
                menuItems: usersOptions,
                label: 'Перевод к',
                id: 'to-user-field',
              }}
            />
            <Field
              name={SUMM}
              component={TextField}
              componentProps={{
                label: 'Сумма',
                lineDirection: 'center',
              }}
            />
            <div className="controls">
              <Button
                raised
                primary
                type="submit"
                disabled={invalid || pristine || submitting}
              >
                Перевести
              </Button>
            </div>
          </form>
        </CardText>
      </Card>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values[TO_USER]) {
    errors[TO_USER] = 'Обязательное поле';
  }
  if (!values[SUMM]) {
    errors[SUMM] = 'Обязательное поле';
  }
  return errors;
}

const form = reduxForm({
  form: TRANSACTIONS_FORM,
  validate,
})(withRouter(TransactionsForm));

const mapStateToProps = (state) => ({
  users: state.usersList.serviceData,
});
const mapDispatchToProps = (dispatch) => ({
  onGetUsers: () => dispatch(getUsersList()),
  onCreateTransaction: () => dispatch(createTransaction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(form)