import React from 'react';
import { withRouter } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { Card, CardTitle, CardText, Button } from 'react-md';

import TextField from '../../fields/TextField';

import { userLogin } from '../../../actions/user/login';
import { required, minLengthPassword, alphaNumeric, email } from "../../../constants/validators";

import {
  SIGN_IN_FORM,
  USERNAME,
  PASSWORD,
  ROUTE_SIGN_UP,
  ROUTE_TRANSACTIONS,
} from '../../../constants';

import './style.scss';

class SignInForm extends React.Component {
  componentWillMount() {
    window.forceUpdate = () => {
      this.forceUpdate();
    }
  }

  onSubmit = () => {
    const {
      onLogin,
      history: { push },
    } = this.props;

    onLogin().then((action) => {
      if (action && action.data) {
        push(ROUTE_TRANSACTIONS);
      }
    });
  }

  render() {
    const { submitting, invalid, pristine, handleSubmit } = this.props;
    return (
      <Card className="sign-in-form">
        <CardTitle title="Вход" />
        <CardText>
          {window.fails >= 3 ? (
            <span className="fail">Вы привысили число попыток, попробуйте позже...</span>
          ): (
            <form onSubmit={handleSubmit(this.onSubmit)}>
            <Field
              name={USERNAME}
              type="text"
              component={TextField}
              // validate={[email, required]}
              componentProps={{
                label: 'Логин',
                lineDirection: 'center',
              }}
            />
            <Field
              name={PASSWORD}
              type="password"
              component={TextField}
              // validate={[minLengthPassword, alphaNumeric, required]}
              componentProps={{
                label: 'Пароль',
                lineDirection: 'center',
              }}
            />
            <div className="controls">
              <NavLink to={ROUTE_SIGN_UP}>
                <Button
                  raised
                  secondary
                >
                  Регистрация
                </Button>
              </NavLink>
              <Button
                raised
                primary
                type="submit"
                disabled={invalid || pristine || submitting}
              >
                Войти
              </Button>
            </div>
          </form>
          )
          }
        </CardText>
      </Card>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values[USERNAME]) {
    errors[USERNAME] = 'Обязательное поле';
  }
  if (!values[PASSWORD]) {
    errors[PASSWORD] = 'Обязательное поле';
  }
  return errors;
}

const form = reduxForm({
  form: SIGN_IN_FORM,
  validate,
})(withRouter(SignInForm));

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
  onLogin: () => dispatch(userLogin()),
});

export default connect(mapStateToProps, mapDispatchToProps)(form)