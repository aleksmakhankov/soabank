/**
 * File for CLI knex commands
 */
const config = require('./data/config.json');

module.exports = config.databaseConfig;
