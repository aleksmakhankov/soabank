exports.up = knex =>
    knex.schema.createTable('payment_history', (table) => {
        table.increments('id').primary();
        table.integer('amount ').unsigned();
        table.integer('source').unsigned().references('accounts.id');
        table.integer('target').unsigned().references('accounts.id');

        table.timestamp('created_at');
        table.timestamp('updated_at');
    });

exports.down = knex =>
    knex.schema.dropTable('payment_history');
