exports.up = knex =>
    knex.schema.createTable('accounts', (table) => {
        table.increments('id').primary();
        table.integer('amount');
        table.integer('user_id').unique().references('users.id');
    });

exports.down = knex =>
    knex.schema.dropTable('accounts');
