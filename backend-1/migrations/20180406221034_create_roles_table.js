exports.up = knex =>
    knex.schema.createTable('roles', (table) => {
        table.increments('id').primary();
        table.string('label').notNull();
        table.integer('access_level').notNull();
    });

exports.down = knex =>
    knex.schema.dropTable('roles');
