exports.up = knex =>
    knex.schema.createTable('users', (table) => {
        table.increments('id').primary();
        table.string('login').notNull().unique();
        table.string('password').notNull();
        table.string('name');
        table.string('company_name');
        table.integer('role').references('roles.id')
    });

exports.down = knex =>
    knex.schema.dropTable('users');
