import { NOT_FOUND } from '../constants/errors';

export default class PermisionDeniedError extends Error {
    status: number;
    message: string;

    constructor(msg, errorData) {
        super(msg);

        this.status = 403;
        this.message = msg || NOT_FOUND;
    }
}