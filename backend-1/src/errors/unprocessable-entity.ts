import { UNPROCESSABLE_ENTITY } from '../constants/errors';

export default class UnprocessableEntityError extends Error {
    status: number;
    message: string;

    constructor(msg, errorData) {
        super(msg);

        this.status = 422;
        this.message = msg || UNPROCESSABLE_ENTITY;
    }
}