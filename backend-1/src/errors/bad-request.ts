import { BAD_REQUEST } from '../constants/errors';

export default class BadRequestError extends Error {
    status: number;
    message: string;
    errorData: Object;

    constructor(msg: string = BAD_REQUEST, errorData: object = {}) {
        super(msg);

        this.status = 400;
        this.message = msg;
        this.errorData = errorData;
    }
}