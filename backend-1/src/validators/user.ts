import * as Joi from 'joi';

export default {
    signIn: () => ({
        body: Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }),
    signUp: () => ({
        body: Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
            name: Joi.string(),
            company_name: Joi.string(),
        }),
    }),
    invite: () => ({
        body: Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
            role: Joi.number().min(1).max(2).required(),
            name: Joi.string(),
            company_name: Joi.string(),
        }),
    }),
};
