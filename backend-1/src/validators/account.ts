import * as Joi from 'joi';

export default {
    charge: () => ({
        body: Joi.object().keys({
            destination: Joi.number().required(),
            amount: Joi.number().required(),
        }),
    }),
    chargeByCompany: () => ({
        body: Joi.object().keys({
            destination: Joi.number().required(),
            amount: Joi.number().required(),
        }),
    }),
};
