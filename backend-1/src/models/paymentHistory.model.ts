import Database from '../helpers/database';
import { User } from '.';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class PaymentHistory extends bookshelf.Model<PaymentHistory> {
    get hasTimestamps() { 
        return true
    }

    get tableName() {
        return 'payment_history';
    }

    source() {
        return this.belongsTo(User, 'source');
    }
    
    destination() {
        return this.belongsTo(User, 'target');
    }
}

export default PaymentHistory;