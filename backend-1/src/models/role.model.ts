import Database from '../helpers/database';
import { User } from '../models';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class Role extends bookshelf.Model<Role> {
    get tableName() {
        return 'roles';
    }

    users() {
        return this.hasMany(User);
    }
}

export default Role;