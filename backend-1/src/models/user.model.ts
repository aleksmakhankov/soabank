import Database from '../helpers/database';
import { Role, Account } from '../models';
import logger from '../helpers/logger';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class User extends bookshelf.Model<User> {
  initialize() {
    this.on('created', model => {
        new Account({ user_id: model.get('id'), amount: 0 }).save();
        logger.info(`Account created for user #${model.get('id')}`);
    });
  }

  get tableName() {
    return 'users';
  }

  role() {
    return this.belongsTo(Role, 'role_id');
  }

  account() {
    return this.hasOne(Account);
  }
}

export default User;
