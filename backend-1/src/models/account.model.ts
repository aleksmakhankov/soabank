import Database from '../helpers/database';
import { User } from './';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class Account extends bookshelf.Model<Account> {
    get tableName() {
        return 'accounts';
    }

    user() {
        return this.belongsTo(User);
    }
}

export default Account;