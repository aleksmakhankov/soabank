import * as crypto from 'crypto-js';
import { User, Role } from '../models';

export default {
    async create(data) {
        
        console.log('data.password: ', data.password);
        var bytes = crypto.AES.decrypt(data.password.toString(), 'secret');
        data.password = bytes.toString(crypto.enc.Utf8);
        console.log('data.password: ', data.password);

        return new User(data).save();
    },

    async findByToken(token) {
        return new User().where({ token }).fetch();
    },
    
    async find(condition) {
        return new User().where(condition).fetch();
    },

    async findAll() {
        return new User().fetchAll({
            columns: [ 'id', 'name', 'company_name', 'role' ],
        });
    },

    async findByCompany(company_name) {
        return new User()
        .where({
            company_name,
        })
        .fetchAll({
            columns: ['id', 'name', 'company_name'],
        });
    },
}