import { User, Role, Account, PaymentHistory } from '../models';
import logger from '../helpers/logger';

export default {
    async getAccountByUserId(user_id) {
        const user = await new User()
        .where({ id: user_id })
        .fetch({ withRelated: ['account']});

        return { ...user.related('account').toJSON(), role: user.get('role'), company_name: user.get('company_name') };
    },

    async getAccounts() {
        return new Account().fetchAll();
    },

    async getCompanyAccount(userId) {
        const user = await new User().where({ id: userId }).fetch();
        if (user.get('role') !== 3) {
            return {};
        }
        const company = await new User()
            .where({
                company_name: user.get('company_name'),
                role: 4,
            })
            .fetch();

        return new Account().where({ user_id: company.get('id') }).fetch();
    },

    async charge({ destination, source, amount }) {
        const sourceAccount = await new Account().where({ user_id: source }).fetch();
        const destinationAccount = await new Account().where({ user_id: destination }).fetch();
        
        const sourceAmount = sourceAccount.get('amount');
        const destinationAmount = destinationAccount.get('amount');
        
        if(sourceAmount >= amount) {
            await sourceAccount.set({ amount: sourceAmount - amount }).save();
            await destinationAccount.set({ amount: amount + destinationAmount }).save();
            
            return new PaymentHistory({
                amount,
                source: sourceAccount.get('id'),
                target: destinationAccount.get('id'),
            }).save();
        } else {
            throw new Error();
        }
    },

    async chargeByCompany (userId, { amount, destination }) {
        const user = await new User().where({ id: userId }).fetch();
        if (user.get('role') !== 3) {
            throw new Error('HAVEN`T ACCESS TO THIS ACTION, NEED ROLE 3');
        }
        const company = await new User()
            .where({
                company_name: user.get('company_name'),
                role: 4,
            })
            .fetch();

        // try charge
        const sourceAccount = await new Account().where({ user_id: company.get('id') }).fetch();
        const destinationAccount = await new Account().where({ user_id: destination }).fetch();

        const sourceAmount = sourceAccount.get('amount');
        const destinationAmount = destinationAccount.get('amount');
        
        if(sourceAmount >= amount) {
            await sourceAccount.set({ amount: sourceAmount - amount }).save();
            await destinationAccount.set({ amount: amount + destinationAmount }).save();
            
            return new PaymentHistory({
                amount,
                source: sourceAccount.get('id'),
                target: destinationAccount.get('id'),
            }).save();
        } else {
            throw new Error();
        }
    },

    async findHistoryByUserId(user_id) {
        console.log('user_id: ', user_id);
        const account = await new Account().where({ user_id }).fetch();

        const payments = await new PaymentHistory().query(qb => {
            qb.select('*').from('payment_history');
            qb.where(function() {
                this.where('payment_history.source', account.get('id'))
            });
            qb.orWhere(function() {
                this.where('payment_history.target', account.get('id'))
            });
        }).fetchAll();

        const data = [];
        for (let i = 0; i < payments.length; i++) {
          const history = payments.at(i);
          
          const source = await new Account().where({ id: history.get('source')}).fetch();  
          const destination = await new Account().where({ id: history.get('target') }).fetch();
          
          const sourceUser = await new User()
            .where({ id: source.get('user_id') })
            .fetch({
              columns: ['id', 'name', 'company_name'],
            });
            const destinationUser = await new User()
            .where({ id: destination.get('user_id') })
            .fetch({
                columns: ['id', 'name', 'company_name'],
            });

            data.push({
                history: history.toJSON(),
                source: sourceUser.toJSON(),
                target: destinationUser.toJSON(),
            });
        }

        return data;
    },
    
    async findHistoryByCompany(user_id) {
        const user = await new User().where({id: user_id}).fetch();
        const role = await new Role().where({id: user.get('role')}).fetch();

        logger.info('role', role.get('access_level'));

        if (role.get('access_level') <= 1) {
            throw new Error('HAVEN`T_ACCESS_TO_THIS_ACTION');
        }

        const users = await new User().where({ company_name: user.get('company_name') }).fetchAll();

        if (users.length <= 0) {
            throw new Error('YOUR_COMPANY_HAVEN`T_USERS');
        }

        const ids = [];

        for (let i = 0; i < users.length; i++) {
            const account = await new Account().where({ user_id: users.at(i).get('id') }).fetch();
            if (account) {
                ids.push(account.get('id'));
            }
        }

        logger.info('ids: ', ids);

        const payments = await new PaymentHistory().query(qb => {
            qb.select('*');
            qb.where(function() {
                this.where('source' as any, 'in', ids as any);
            });
            qb.orWhere(function() {
                this.where('target' as any, 'in', ids as any);
            });
        }).fetchAll();

        const data = [];
        for (let i = 0; i < payments.length; i++) {
          const history = payments.at(i);
          
          const source = await new Account().where({ id: history.get('source')}).fetch();  
          const destination = await new Account().where({ id: history.get('target') }).fetch();
          
          const sourceUser = await new User()
            .where({ id: source.get('user_id') })
            .fetch({
              columns: ['id', 'name', 'company_name'],
            });
            const destinationUser = await new User()
            .where({ id: destination.get('user_id') })
            .fetch({
                columns: ['id', 'name', 'company_name'],
            });

            data.push({
                history: history.toJSON(),
                source: sourceUser.toJSON(),
                target: destinationUser.toJSON(),
            });
        }

        return data;
    }
}