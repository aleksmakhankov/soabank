import { Role } from '../models';

export default {
    async findById(id) {
        return new Role().where({ id }).fetch();
    },
}