import logger from '../helpers/logger';

export default (error, req, res, next) => {
    if (res.headersSent) {
        return next(error);
    }

    if (error.code === 'LIMIT_UNEXPECTED_FILE') {
        error.status = 422;
        // TODO: add errors constants
        error.message = 'LIMIT';
    }

    res.status(error.status || 500);

    // TODO: use constant
    const errorMessage = error.message;

    // TODO: maybe scheme
    const response: any = { error: errorMessage };

    if (error.errorData) {
        response.errorData = error.errorData;
    }

    res.send(response);

    logger.error(error.stack.toString());
};
