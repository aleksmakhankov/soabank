import * as jwt from 'jsonwebtoken';
import * as expressJwt from 'express-jwt';
import config from '../helpers/config';

const secret = config.secret.token;
const authenticate = expressJwt({ secret });

function generateToken(req, res, next) {
    req.token = jwt.sign({
        id: req.user.id,
    }, secret);
    next();
};

export {
    authenticate,
    generateToken
};
