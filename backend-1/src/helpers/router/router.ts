import { Router as BaseRouter, Request, Response } from 'express';

import asyncMiddleware from '../../middlewares/async.middleware';

export default class Router {
    router: BaseRouter;

    constructor(...args) {
        this.router = BaseRouter(...args);
    }

    getRouter() {
        return this.router;
    }

    get(path, ...middlewares) {
        this.router.get(path, ...this.wrapMiddlewares(middlewares));
    }

    post(path, ...middlewares) {
        this.router.post(path, ...this.wrapMiddlewares(middlewares));
    }

    put(path, ...middlewares) {
        this.router.put(path, ...this.wrapMiddlewares(middlewares));
    }

    delete(path, ...middlewares) {
        this.router.delete(path, ...this.wrapMiddlewares(middlewares));
    }

    wrapMiddlewares(middlewares) {
        return middlewares.map(fn => asyncMiddleware(fn));
    }
};
