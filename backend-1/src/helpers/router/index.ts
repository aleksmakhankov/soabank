import { Request, Response } from 'express';

import Router from '../../helpers/router/router';
import routes from '../../routes';

const router: Router = new Router();

 // Connect api routes
Object.values(routes).forEach(route => route(router));

// Common router
router.get('/*', (req: Request, res: Response) => {
    // TODO: send const json
    res.json({ error: 'NOT_FOUND' });
});

export default router;