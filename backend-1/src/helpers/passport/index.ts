import * as passport from 'passport';
import * as LocalStrategy from 'passport-local';
import * as crypto from 'crypto-js';

import logger from '../../helpers/logger';
import { User } from '../../models';
import { USER_SAFE_COLUMNS } from '../../constants/columns';

passport.use(new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password'
},
    async function (login, password, done) {
        const bytes = crypto.AES.decrypt(password.toString(), 'secret');
        const formattedPassword = bytes.toString(crypto.enc.Utf8);
        
        const user = await new User().where({ login, password: formattedPassword }).fetch({ columns: USER_SAFE_COLUMNS });
        
        logger.info('Try auth:', { login, hash: passport })

        if (!user) {
            return done(null, false);
        }

        return await done(null, await user.toJSON());
    })
);

export default passport;