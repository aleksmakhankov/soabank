const USER_SAFE_COLUMNS = ['id', 'login', 'role', 'name', 'company_name'];

export {
    USER_SAFE_COLUMNS
};
