import helper from '../controllers/_controllerHelper';
import repository from '../repositories/user.repository';
import roleRepository from '../repositories/role.repository';

export default {
    authorization,
    registration,
    invite,
    list,
    listCompany,
    registrationCompany,
};

async function authorization(req, res) {
    const { user, token } = req;
    const role = await roleRepository.findById(user.role);
    helper.sendData({ user, role, token }, res);
}

async function registration(req, res) {
    const { body: data } = req;

    const result = await repository.create({...data, role: 1});
    helper.sendData(result, res);
}

async function registrationCompany(req, res) {
    const { body: data } = req;
    let result = {};
    try {
        result = await repository.create({...data, role: 4});
    } catch (e) {
        result = await repository.find({ company_name: data.company_name });
    }

    helper.sendData(result, res);
}

async function invite(req, res) {
    const { body: data } = req;

    const result = await repository.create(data);
    helper.sendData({ login: result.get('login') }, res);
}

async function list(_req, res) {
    const result = await repository.findAll();
    helper.sendData(result, res);
}

async function listCompany(req, res) {
    const { user: userReq } = req;

    const user = await repository.find({ id: userReq.id });

    if (user.get('role') <= 1) {
        throw new Error('ACCESS_DENIED');
    }

    const result = await repository.findByCompany(user.get('company_name'));

    helper.sendData(result, res);
}

