import helper from './_controllerHelper';
import repository from '../repositories/account.repository';
import roleRepository from '../repositories/role.repository';
import logger from '../helpers/logger';

export default {
    account,
    charge,
    chargeByCompany,
    history,
    historyByCompany,
};

async function account(req, res) {
    const { user } = req;
    const data = await repository.getAccountByUserId(user.id);

    data.company = await repository.getCompanyAccount(user.id);
    
    helper.sendData(data, res);
}

async function charge(req, res) {
    const { user, body: data } = req;

    const result = await repository.charge({ source: user.id, ...data });

    helper.sendData(result, res);
}

async function chargeByCompany(req, res) {
    const { user, body: data } = req;

    const result = await repository.chargeByCompany(user.id, data);

    helper.sendData(result, res);
}

async function history(req, res) {
    const { user } = req;
    const history = await repository.findHistoryByUserId(user.id);

    helper.sendData(history, res);
}

async function historyByCompany(req, res) {
    const { user } = req;
    const history = await repository.findHistoryByCompany(user.id);

    helper.sendData(history, res);
}
