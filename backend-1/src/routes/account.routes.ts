import validator from '../middlewares/validator';
import rules from '../validators/account';
import controller from '../controllers/account.controller';
import { authenticate } from '../middlewares/token.middleware';

export default function (app) {
    app.get('/account',
        authenticate,
        controller.account,
    );

    app.post('/charge',
        authenticate,
        validator(rules.charge),
        controller.charge,
    );
    
    app.post('/charge-by-company',
        authenticate,
        validator(rules.chargeByCompany),
        controller.chargeByCompany,
    );

    app.get('/payment-history',
        authenticate,
        controller.history,
    );
    
    app.get('/payment-history-company',
        authenticate,
        controller.historyByCompany,
    );
}