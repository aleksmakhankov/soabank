import users from '../routes/user.routes';
import account from '../routes/account.routes';

export default {
    users,
    account,
};
