import validator from '../middlewares/validator';
import rules from '../validators/user';
import controller from '../controllers/user.controller';
import passport from '../helpers/passport';
import { generateToken, authenticate } from '../middlewares/token.middleware';

export default function (app) {
    app.post('/authorization',
        validator(rules.signIn),
        passport.authenticate('local', { session: false }),
        generateToken,
        controller.authorization
    );
    
    app.post('/registration',
        validator(rules.signUp),
        controller.registration,
    );

    app.post('/registration-company',
        authenticate,
        validator(rules.signUp),
        controller.registrationCompany,
    );

    app.post('/invite',
        authenticate,
        validator(rules.invite),
        controller.invite,
    );

    app.get('/list',
        authenticate,
        controller.list,
    );

    app.get('/list-by-company',
        authenticate,
        controller.listCompany,
    );
}