exports.up = knex =>
    knex.schema.createTable('companies', (table) => {
        table.increments('id').primary();
        table.string('company_name').notNull().unique();
        table.string('company_id').notNull().unique();
    });

exports.down = knex =>
    knex.schema.dropTable('companies');
