exports.up = knex =>
    knex.schema.createTable('vacations', (table) => {
        table.increments('id').primary();
        table.string('user_id').notNull();
        table.string('from').notNull();
        table.string('to').notNull();
        table.boolean('paid');
    });

exports.down = knex =>
    knex.schema.dropTable('vacations');
