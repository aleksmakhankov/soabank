exports.up = knex =>
    knex.schema.createTable('employees', (table) => {
        table.increments('id').primary();
        table.string('first_name').notNull();
        table.string('last_name').notNull();
        table.integer('user_id').notNull();
        table.integer('rate').notNull();
    });

exports.down = knex =>
    knex.schema.dropTable('employees');
