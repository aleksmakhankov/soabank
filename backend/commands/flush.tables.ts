#!/usr/bin/env node
const Knex = require('knex');
import config from '../src/helpers/config';

const knex = Knex(config.databaseConfig);

knex
    .raw('DROP SCHEMA public CASCADE;')
    .then(() => knex.raw('CREATE SCHEMA public;'))
    .finally(knex.destroy);
