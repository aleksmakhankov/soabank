import * as express from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as _ from 'lodash';

import config from './helpers/config';
import router from './helpers/router';
import pathHelper from './helpers/pathHelper';
import passport from './helpers/passport';

// middlewares
import errorHandlerMiddleware from './middlewares/error.middleware';


const app = express();

export default {
    start
};

function start(port) {
    initExpress();

    if (config.isDevLocal) {
        app.use(morgan('dev'));
    }

    return new Promise((resolve, reject) => {
        app.listen(port, () => {
            return resolve(port);
        });
    });
}

function initExpress() {
    if (config.isDevLocal) app.use(morgan('dev')); //log requests

    app.use(bodyParser.json()); // get information from html forms
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(passport.initialize());

    app.use('/', express.static(pathHelper.getClientRelative('/'), { index: false }));
    app.use('/api', router.getRouter());

    app.use(errorHandlerMiddleware);

    app.use(cors());

}
