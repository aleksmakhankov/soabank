import Database from '../helpers/database';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class Vacation extends bookshelf.Model<Vacation> {

  get tableName() {
    return 'vacations';
  }
}

export default Vacation;
