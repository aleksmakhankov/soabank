import Database from '../helpers/database';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class Employee extends bookshelf.Model<Employee> {

  get tableName() {
    return 'employees';
  }
}

export default Employee;
