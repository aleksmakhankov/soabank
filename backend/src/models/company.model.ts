import Database from '../helpers/database';

const instance = Database.getInstance();
const bookshelf = instance.getBookshelf();

class Employee extends bookshelf.Model<Employee> {

  get tableName() {
    return 'companies';
  }
}

export default Employee;
