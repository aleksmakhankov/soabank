import User from '../models/user.model';
import Employee from '../models/employee.model';
import Company from '../models/company.model';
import Vacation from '../models/vacation.model';

export {
    User,
    Employee,
    Company,
    Vacation,
};
