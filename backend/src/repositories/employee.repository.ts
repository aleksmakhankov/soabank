import { Employee } from '../models';

export default {
    async create(data) {
        return new Employee(data).save();
    },
    
    async find(condition) {
        return new Employee().where(condition).fetch();
    },

    async findAll() {
        return new Employee().fetchAll();
    }
}