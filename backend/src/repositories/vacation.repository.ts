import { Vacation } from '../models';

export default {
    async create(data) {
        return new Vacation(data).save();
    },
    
    async update(id, data) {
        return new Vacation({ id }).save(data, { method: 'update' });
    },
    
    async find(condition) {
        return new Vacation().where(condition).fetch();
    },

    async findAll(condition?) {
        if (condition) {
            return new Vacation().where(condition).fetchAll();
        }
        return new Vacation().fetchAll();
    },
}
