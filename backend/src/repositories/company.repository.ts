import { Company } from '../models';

export default {
    async create(data) {
        return new Company(data).save();
    },
    
    async find(condition) {
        return new Company().where(condition).fetch();
    }
}