import axios from 'axios';

import helper from '../controllers/_controllerHelper';
import repository from '../repositories/employee.repository';
import companyRepository from '../repositories/company.repository';
import vacationRepository from '../repositories/vacation.repository';

import logger from '../helpers/logger';

import { API_FIRST_SERVER_ENDPOINT } from '../constants/env';
import employeeRepository from '../repositories/employee.repository';

export default {
    authorization,
    registration,
    account,
    addVacation,
    getVacations,
    getEmployees,
};

async function authorization(req, res) {
    const { body: data } = req;
    
    const { data: responseData } = await axios.post(`${API_FIRST_SERVER_ENDPOINT}/authorization`, {
        login: data.login,
        password: data.password,
    });

    const employee = await repository.find({ user_id: responseData.data.user.id });

    helper.sendData({
        employee,
        user: responseData.data.user,
        token: responseData.data.token,
        role: responseData.data.role,
    }, res);
}

async function registration(req, res) {
    const { body: data } = req;

    // create user on first server
    const { data: responseData } = await axios.post(`${API_FIRST_SERVER_ENDPOINT}/registration`, {
        login: data.login,
        password: data.password,
        company_name: data.company_name,
        name: `${data.first_name} ${data.last_name}`,
    });

    const employee = await repository.create({
        first_name: data.first_name,
        last_name: data.last_name,
        user_id: responseData.data.id,
        rate: data.rate,
    });

    // create company
    const { data: authData } = await axios.post(`${API_FIRST_SERVER_ENDPOINT}/authorization`, {
        login: data.login,
        password: data.password,
    });
    
    let company = {};
    try {
        const { data: companyData } = await axios.post(`${API_FIRST_SERVER_ENDPOINT}/registration-company`, {
            login: data.company_name,
            password: data.password + ~~(Math.random() * 200),
            name: '-',
            company_name: data.company_name,
        }, {
            headers: {
                ['Authorization']: `Bearer ${authData.data.token}`,
            }
        });
        
        company = await companyRepository.create({
            company_name: companyData.data.company_name,
            company_id: companyData.data.id,
        });
        
    } catch(e) {
        company = await companyRepository.find({
            company_name: data.company_name,
        });
    }

    helper.sendData({ 
        ...responseData.data,
        ...employee.toJSON(),
        company,
    }, res);
}

async function account(req, res) {
    const { body: data } = req;
    const { token } = data;

    const { data: companyData } = await axios.get(`${API_FIRST_SERVER_ENDPOINT}/account`, {
        headers: {
            ['Authorization']: `Bearer ${token}`,
        }
    });

    helper.sendData(companyData, res);
}

async function addVacation(req, res) {
    const { body: { token, id, ...data } } = req;

    const { data: accountData } = await axios.get(`${API_FIRST_SERVER_ENDPOINT}/account`, {
        headers: {
            ['Authorization']: `Bearer ${token}`,
        }
    });

    if (accountData.success === false) {
        throw new Error('Authorization failed');
    }

    if (id) {
        const vacation = await vacationRepository.update(id, data);
        helper.sendData(vacation, res);
    } else {
        const vacation = await vacationRepository.create(data);
        helper.sendData(vacation, res);
    }

}

async function getVacations(req, res) {
    const { body: { token } } = req;

    const { data: accountData } = await axios.get(`${API_FIRST_SERVER_ENDPOINT}/account`, {
        headers: {
            ['Authorization']: `Bearer ${token}`,
        }
    });

    if (accountData.success === false) {
        throw new Error('Authorization failed');
    }

    console.log('accountData: ', accountData);
    console.log('accountData user: ', accountData.data.user_id);

    if (accountData.data.role === 3) {
        const vacations = await vacationRepository.findAll();
        helper.sendData(vacations, res);
    } else {
        const vacations = await vacationRepository.findAll({ user_id: accountData.data.user_id });
        helper.sendData(vacations, res);
    }
}

async function getEmployees(req, res) {
    const { body: { token } } = req;

    const { data: accountData } = await axios.get(`${API_FIRST_SERVER_ENDPOINT}/account`, {
        headers: {
            ['Authorization']: `Bearer ${token}`,
        }
    });

    if (accountData.success === false) {
        throw new Error('Authorization failed');
    }

    console.log('accountData: ', accountData);
    console.log('accountData user: ', accountData.data.user_id);

    if (accountData.data.role === 3) {
        const employees = await employeeRepository.findAll();
        helper.sendData(employees, res);
    } else {
        const employee = await employeeRepository.find({ user_id: accountData.data.user_id });
        helper.sendData([employee], res);
    }
}

