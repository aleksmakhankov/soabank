import { NOT_FOUND } from '../constants/errors';

export default class NotFoundError extends Error {
    status: number;
    message: string;

    constructor(msg, errorData) {
        super(msg);

        this.status = 404;
        this.message = msg || NOT_FOUND;
    }
}