import { UNAUTHORIZED } from '../constants/errors';

export default class UnauthorizedError extends Error {
    status: number;
    message: string;

    constructor(msg = UNAUTHORIZED) {
        super(msg);

        this.status = 401;
        this.message = msg;
    }
}