import { FORBIDDEN } from '../constants/errors';

export default class BadRequestError extends Error {
    status: number;
    message: string;

    constructor(msg, errorData) {
        super(msg);

        this.status = 403;
        this.message = msg || FORBIDDEN;
    }
}