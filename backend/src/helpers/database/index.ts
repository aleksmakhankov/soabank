import * as Knex from 'knex';
import * as Bookshelf from 'bookshelf';
import config from '../../helpers/config';

export default class Database {
    private static instance: Database = new Database();

    protected knex;
    protected bookshelf: Bookshelf;

    constructor() {
        if (!Database.instance) {
            this.knex = Knex(config.databaseConfig);
            this.bookshelf = Bookshelf(this.knex);

            Database.instance = this;
        } else {
            throw new Error('Error: Instantiation failed: Use Database.getInstance() instead of new.');
        }
    }

    public static getInstance(): Database {
        return Database.instance;
    }

    public getKnex(): Knex {
        return this.knex;
    }

    public getBookshelf(): Bookshelf {
        return this.bookshelf;
    }
}