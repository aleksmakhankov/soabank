import * as Joi from 'joi';

export default {
    vacation: () => ({
        body: Joi.object().keys({
            token: Joi.string().required(),
            from: Joi.string(),
            to: Joi.string(),
            paid: Joi.boolean(),
            user_id: Joi.number(),
            id: Joi.number(),
        }),
    }),
    account: () => ({
        body: Joi.object().keys({
            token: Joi.string().required(),
        }),
    }),
    signIn: () => ({
        body: Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }),
    signUp: () => ({
        body: Joi.object().keys({
            login: Joi.string().required(),
            password: Joi.string().required(),
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            company_name: Joi.string().required(),
            rate: Joi.number().required(),
        }),
    }),
};
