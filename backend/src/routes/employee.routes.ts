import validator from '../middlewares/validator';
import rules from '../validators/employee';
import controller from '../controllers/user.controller';

export default function (app) {
    app.post('/authorization',
        validator(rules.signIn),
        controller.authorization
    );
    
    app.post('/registration',
        validator(rules.signUp),
        controller.registration,
    );
    
    app.post('/account',
        validator(rules.account),
        controller.account,
    );
    
    app.post('/vacation',
        validator(rules.vacation),
        controller.addVacation,
    );
    
    app.post('/vacations',
        validator(rules.account),
        controller.getVacations,
    );
    
    app.post('/employees',
        validator(rules.account),
        controller.getEmployees,
    );
}