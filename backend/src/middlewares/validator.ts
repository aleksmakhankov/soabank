import * as _ from 'lodash';
import * as Joi from 'Joi';
import logger from '../helpers/logger';
import { BadRequestError } from '../errors';
import { VALIDATOR_ERROR } from '../constants/messages.error';

export default rules => async (req, res, next) => {
    const data: any = {};

    if (!['GET', 'DELETE'].includes(req.method)) {
        data.body = req.body;
    }

    if (!_.isEmpty(req.params)) {
        data.params = req.params;
    }
    if (!_.isEmpty(req.query)) {
        data.query = req.query;
    }

    const result = Joi.validate(data, rules(), {
        abortEarly: false,
        stripUnknown: false
    });

    if (result.error) {
        const errors = {};
        result.error.details.forEach((e) => {
            if (!errors[e.path]) {
                errors[e.path] = [];
            }
            errors[e.path].push({
                type: e.type,
                message: e.message
            });
        });

        logger.error(`validator: ${JSON.stringify(errors, null, 4)}`);
        throw new BadRequestError(VALIDATOR_ERROR);
    } else {
        next();
    }

};
